#include "MonteCarlo.h"

sir_rates GetEvolution ( char* evolution, int steps, long N )
{
     sir_rates R;
     R.I = ( long* ) calloc ( steps, sizeof ( long ) );
     R.R = ( long* ) calloc ( steps, sizeof ( long ) );
     R.S = ( long* ) calloc ( steps, sizeof ( long ) );

     for ( long t=0; t<steps; t++ ) {
          for ( long i=0; i<N; i++ ) {
               if ( evolution[t*N+i] == INFECTED ) {
                    R.I[t]++;
               } else if ( evolution[t*N+i] == SUSCEPTIBLE ) {
                    R.S[t]++;
               } else if ( evolution[t*N+i] == RECOVERED ) {
                    R.R[t]++;
               }
          }
     }

     return R;
}

void FreeRateSIR ( sir_rates *R )
{
     free ( R->I );
     free ( R->S );
     free ( R->R );

}

sir_rates* GetEvolutionLayer ( char* evolution, int steps, long N, long L )
{
     sir_rates *R = ( sir_rates* ) calloc ( L+1, sizeof ( sir_rates ) );

     for ( long i=0; i<L+1; i++ ) {
          R[i].I = ( long* ) calloc ( steps, sizeof ( long ) );
          R[i].R = ( long* ) calloc ( steps, sizeof ( long ) );
          R[i].S = ( long* ) calloc ( steps, sizeof ( long ) );
     }

     for ( long t=0; t<steps; t++ ) {
          for ( long m=0; m<L; m++ ) {
               for ( long i=0; i<N; i++ ) {
                    // Layer behavior
                    if ( evolution[t*N*L +i +m*N] == INFECTED ) {
                         R[m].I[t]++;
                    } else if ( evolution[t*N*L +i +m*N] == SUSCEPTIBLE ) {
                         R[m].S[t]++;
                    } else if ( evolution[t*N*L +i +m*N] == RECOVERED ) {
                         R[m].R[t]++;
                    }

                    // Global behavior
                    if ( evolution[t*N*L +i +m*N] == INFECTED ) {
                         R[L].I[t]++;
                    } else if ( evolution[t*N*L +i +m*N] == SUSCEPTIBLE ) {
                         R[L].S[t]++;
                    } else if ( evolution[t*N*L +i +m*N] == RECOVERED ) {
                         R[L].R[t]++;
                    }
               }
          }
     }

     return R;
}


void FreeRateSIRLayer ( sir_rates *R, long L )
{
     for ( long i=0; i<L+1; i++ ) {
          free ( R[i].I );
          free ( R[i].S );
          free ( R[i].R );
     }

     free ( R );

}


// RP
char* MonteCarlo_SIS_RP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              for ( long j=0; j< ( long ) M.ML[m][i].size(); j++ ) {
                                   if ( M.ML[m][i][j].second == m ) { // same layer
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }

                                   } else { // different layers
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < eta ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }
                                   }
                              }

                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = SUSCEPTIBLE;
                                   Ninf--;
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}

char* MonteCarlo_SIS_RP_Reinfection ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {

               // recovering
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = SUSCEPTIBLE;
                                   Ninf--;
                              }
                         }
                    }
               }

               // spreding WITH reinfection!!!
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {

                              for ( long j=0; j< ( long ) M.ML[m][i].size(); j++ ) {
                                   if ( M.ML[m][i][j].second == m ) { // same layer
                                        if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                             if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                  evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                  Ninf++;
                                             }
                                        }


                                   } else { // different layers
                                        if ( gsl_rng_uniform ( randomic ) < eta ) {
                                             if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                  evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                  Ninf++;
                                             }
                                        }

                                   }
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}


char* MonteCarlo_SIR_RP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              for ( long j=0; j< ( long ) M.ML[m][i].size(); j++ ) {
                                   if ( M.ML[m][i][j].second == m ) { // same layer

                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }

                                   } else { // different layers
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < eta ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }
                                   }
                              }

                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = RECOVERED;
                                   Ninf--;
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}


// CP
char* MonteCarlo_SIS_CP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              long j = gsl_rng_uniform_int ( randomic, ( long ) M.ML[m][i].size() );

                              {
                                   if ( M.ML[m][i][j].second == m ) { // same layer
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }

                                   } else { // different layers
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < eta ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }
                                   }
                              }

                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = SUSCEPTIBLE;
                                   Ninf--;
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}

char* MonteCarlo_SIS_CP_Reinfection ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {

               // recovering
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = SUSCEPTIBLE;
                                   Ninf--;
                              }
                         }
                    }
               }

               // spreading WITH reinfection!!!
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {

                              long j = gsl_rng_uniform_int ( randomic, ( long ) M.ML[m][i].size() );

                              {
                                   if ( M.ML[m][i][j].second == m ) { // same layer
                                        if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                             if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                  evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                  Ninf++;
                                             }
                                        }


                                   } else { // different layers
                                        if ( gsl_rng_uniform ( randomic ) < eta ) {
                                             if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                  evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                  Ninf++;
                                             }
                                        }

                                   }
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}


char* MonteCarlo_SIR_CP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic )
{
     long N = M.N;
     long L = M.M;

     char *evolution = ( char* ) calloc ( N*L*steps, sizeof ( char ) );

     long Ninf = 0;
     for ( long i=0; i<N*L; i++ ) {
          evolution[i] = initial[i];
          if ( initial[i] == INFECTED ) {
               Ninf++;
          }
     }

     for ( int t=1; t<steps; t++ ) {
          for ( long i=0; i<N*L; i++ ) {
               evolution[t*N*L+i] = evolution[ ( t-1 ) *N*L+i];
          }

          if ( Ninf > 0 ) {
               for ( long m=0; m<M.M; m++ ) {
                    for ( long i=0; i<N; i++ ) {
                         if ( evolution[ ( t-1 ) *N*L + i + N*m] == INFECTED ) {
                              long j = gsl_rng_uniform_int ( randomic, ( long ) M.ML[m][i].size() );

                              {
                                   if ( M.ML[m][i][j].second == m ) { // same layer

                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < lambda ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*m )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }

                                   } else { // different layers
                                        if ( evolution[ ( t-1 ) *N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) {
                                             if ( gsl_rng_uniform ( randomic ) < eta ) {
                                                  if ( evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] == SUSCEPTIBLE ) { // still SUSCEPTIBLE in t (have not been infected yet)
                                                       evolution[t*N*L + ( long ) ( M.ML[m][i][j].first + N*M.ML[m][i][j].second )] = INFECTED;
                                                       Ninf++;
                                                  }
                                             }
                                        }
                                   }
                              }

                              if ( gsl_rng_uniform ( randomic ) < delta ) {
                                   evolution[t*N*L +i +N*m] = RECOVERED;
                                   Ninf--;
                              }
                         }
                    }
               }
          }
     }


     return evolution;

}


void MonteCarlo_Aux ( Multilayer &Ml, int Nexp, double y0, int steps, double lambda, double eta, double delta, gsl_rng * randomic, int type, char *fname )
{
     long N = Ml.N;
     long M = Ml.M;

     // auxuliary vectors
     double **Exps_I = ( double** ) calloc ( M+1, sizeof ( double ) );
     double **Exps_S = ( double** ) calloc ( M+1, sizeof ( double ) );
     double **Exps_I0 = ( double** ) calloc ( M+1, sizeof ( double ) );
     double **Exps_S0 = ( double** ) calloc ( M+1, sizeof ( double ) );

     double **Exps_R = ( double** ) calloc ( M+1, sizeof ( double ) );
     double **Exps_R0 = ( double** ) calloc ( M+1, sizeof ( double ) );

     for ( long i=0; i<M+1; i++ ) {
          Exps_I[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );
          Exps_S[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );
          Exps_I0[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );
          Exps_S0[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );

          Exps_R[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );
          Exps_R0[i] = ( double* ) calloc ( Nexp, sizeof ( double ) );
     }

     // Individual Statistics file
     ofstream out_ind ( fname );

     // Dynamics initialization
     char *initial = ( char* ) calloc ( N*M, sizeof ( char ) );

     for ( long k=0; k<Nexp; k++ ) {
          for ( long i=0; i<N*M; i++ ) {
               if ( gsl_rng_uniform ( randomic ) < y0 ) {
                    initial[i] = INFECTED;
                    
               } else {
                    initial[i] = SUSCEPTIBLE;

               }
          }

          char *evo = NULL;

          if ( type == 0 ) {
               evo = MonteCarlo_SIS_RP ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          } else if ( type == 1 ) {
               evo = MonteCarlo_SIS_RP_Reinfection ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          } else if ( type == 2 ) {
               evo = MonteCarlo_SIR_RP ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          } else if ( type == 3 ) {
               evo = MonteCarlo_SIS_CP ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          } else  if ( type == 4 ){
               evo = MonteCarlo_SIS_CP_Reinfection ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          } else {
               evo = MonteCarlo_SIR_CP ( Ml, steps, lambda, eta, delta, initial, randomic );
               
          }

          sir_rates *Rates = GetEvolutionLayer ( evo, steps, N, M );

          // global + layer
          for ( long i=0; i<M+1; i++ ) {
               Exps_I[i][k] = Rates[i].I[steps-1];
               Exps_S[i][k] = Rates[i].S[steps-1];

               Exps_I0[i][k] = Rates[i].I[0];
               Exps_S0[i][k] = Rates[i].S[0];

               Exps_R[i][k] = Rates[i].R[steps-1];
               Exps_R0[i][k] = Rates[i].R[0];

               out_ind << Rates[i].S[steps-1] << "\t" << Rates[i].I[steps-1]  << "\t" << Rates[i].R[steps-1];

               if ( i < M ) {
                    out_ind << "\t";
               }
          }

          out_ind << endl;

          FreeRateSIRLayer ( Rates, M );
          free ( evo );
     }

     out_ind.close();
     
     // Calculating statistics from data
     // Commented
     double *mean_I = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_I = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *mean_S = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_S = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *mean_I0 = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_I0 = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *mean_S0 = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_S0 = ( double* ) calloc ( M+1, sizeof ( double ) );

     double *mean_R = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_R = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *mean_R0 = ( double* ) calloc ( M+1, sizeof ( double ) );
     double *sd_mean_R0 = ( double* ) calloc ( M+1, sizeof ( double ) );

     for ( long i=0; i<M+1; i++ ) {
          mean_I[i] = gsl_stats_mean ( Exps_I[i], 1, Nexp );
          sd_mean_I[i] = gsl_stats_sd ( Exps_I[i], 1, Nexp );

          mean_S[i] = gsl_stats_mean ( Exps_S[i], 1, Nexp );
          sd_mean_S[i] = gsl_stats_sd ( Exps_S[i], 1, Nexp );

          mean_I0[i] = gsl_stats_mean ( Exps_I0[i], 1, Nexp );
          sd_mean_I0[i] = gsl_stats_sd ( Exps_I0[i], 1, Nexp );

          mean_S0[i] = gsl_stats_mean ( Exps_S0[i], 1, Nexp );
          sd_mean_S0[i] = gsl_stats_sd ( Exps_S0[i], 1, Nexp );



          mean_R[i] = gsl_stats_mean ( Exps_R[i], 1, Nexp );
          sd_mean_R[i] = gsl_stats_sd ( Exps_R[i], 1, Nexp );

          mean_R0[i] = gsl_stats_mean ( Exps_R0[i], 1, Nexp );
          sd_mean_R0[i] = gsl_stats_sd ( Exps_R0[i], 1, Nexp );
     }

     // showing and saving satatistics
     for ( long i=0; i<M+1; i++ ) {
          if ( i == M ) {
               cout << "Global:" << endl;
               
          } else {
               cout << "Layer " << i << ":" << endl;

          }

          cout << mean_S0[i] << "\t\t" << sd_mean_S0[i] << "\t\t" << mean_I0[i] << "\t\t" << sd_mean_I0[i] << "\t\t" << mean_R0[i] << "\t\t" << sd_mean_R0[i] << "\t\t" << mean_I0[i]+mean_R0[i]+mean_S0[i] << endl;
          cout << mean_S[i] << "\t\t" << sd_mean_S[i] << "\t\t" << mean_I[i] << "\t\t" << sd_mean_I[i] << "\t\t" << mean_R[i] << "\t\t" << sd_mean_R[i] << "\t\t"  << mean_I[i]+mean_R[i]+mean_S[i] << endl;
          cout << mean_S[i]/ ( ( double ) N*M ) << "\t" << sd_mean_S[i]/ ( ( double ) N*M ) << "\t" << mean_I[i]/ ( ( double ) N*M ) << "\t" << sd_mean_I[i]/ ( ( double ) N*M ) << "\t" << mean_R[i]/ ( ( double ) N*M ) << "\t" << sd_mean_R[i]/ ( ( double ) N*M ) << "\t" << ( mean_I[i]+mean_S[i]+mean_R[i] ) / ( ( double ) N*M ) << endl;
          cout << endl;

     }
     

     // deleting variables
     free ( initial );

     free ( mean_I );
     free ( sd_mean_I );
     free ( mean_S );
     free ( sd_mean_S );
     free ( mean_I0 );
     free ( sd_mean_I0 );
     free ( mean_S0 );
     free ( sd_mean_S0 );

     free ( mean_R );
     free ( sd_mean_R );
     free ( mean_R0 );
     free ( sd_mean_R0 );

     for ( long i=0; i<M+1; i++ ) {
          free ( Exps_I[i] );
          free ( Exps_S[i] );

          free ( Exps_I0[i] );
          free ( Exps_S0[i] );

          free ( Exps_R[i] );
          free ( Exps_R0[i] );
     }

     free ( Exps_I );
     free ( Exps_S );

     free ( Exps_I0 );
     free ( Exps_S0 );

     free ( Exps_R );
     free ( Exps_R0 );
}
