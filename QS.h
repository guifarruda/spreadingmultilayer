#ifndef QS_H
#define QS_H

#include "Defines.h"

#include <iostream>
#include <vector>
#include <algorithm>

#include <ctime>
#include <unistd.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>

#include "Multilayer.h"
#include "ContinuousProcesses.h"

// General SIS QS simulation
int QS_Adaptative_GSIS ( Multilayer &M, double i0, double alpha, double lambda, double eta, double delta, double QSpr, double QStr, double QSta, long QSM, gsl_rng * randomic, double &X, double &rho, vector<double> &X2, vector<double> &rho2, string fname );

// General CP QS simualtions
int QS_Adaptative_GCP ( Multilayer &M, double i0, double alpha, double lambda, double eta, double delta, double QSpr, double QStr, double QSta, long QSM, gsl_rng * randomic, double &X, double &rho, vector<double> &X2, vector<double> &rho2, string fname );

#endif
