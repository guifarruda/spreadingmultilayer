#include "QS.h"


int QS_Adaptative_GSIS ( Multilayer &M, double i0, double alpha, double lambda, double eta, double delta, double QSpr, double QStr, double QSta, long QSM, gsl_rng * randomic, double &X, double &rho, vector<double> &X2, vector<double> &rho2, string fname )
{
    // time
    time_t tic, toc;

    // Multilayer properties
    long n = M.N;
    long l = M.M;
    long m = M.E;

    // Data
    long np = n*l+m;

    // Auxiliary variables
    long Ni = 0;

    // Layer statistics
    vector <vector <double> > barPm ( M.M );
    vector <long> mData_i ( l ), mData_s ( l );
    for ( long k=0; k<l; k++ ) {
        mData_i[k] = 0;
        mData_s[k] = 0;

        barPm[k] = vector <double> ( M.N,0 );
    }


    // Epidemic initialization
    vector <char> processes_state ( n*l );

    do {
        Ni = 0;
        for ( long k=0; k<l; k++ ) {
            for ( long i=0; i<n; i++ ) {
                if ( gsl_rng_uniform ( randomic ) < i0 ) {
                    processes_state[i+k*n] = INFECTED;

                    mData_i[k]++;

                    Ni++;

                } else {
                    processes_state[i+k*n] = SUSCEPTIBLE;

                    mData_s[k]++;
                }
            }
        }
    } while ( Ni == 0 );


    // new time
    // Processes initialization
    vector < pair<long, double> > processes_time ( 0 );
    vector <double> processes_lambda ( np );

    // Lambda rate
    for ( long i=0; i<m; i++ ) {
        if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
            gsl_sf_result result;
            gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
            processes_lambda[i] = 1.0/ ( result.val*lambda );

            if ( result.err > 10e-3 ) {
                cout << "Error > 10e-3: "<< result.err  << endl;
            }


        } else {
            gsl_sf_result result;
            gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
            processes_lambda[i] = 1.0/ ( result.val*eta );

            if ( result.err > 10e-3 ) {
                cout << "Error > 10e-3: " << result.err << endl;
            }

        }
    }

    // Delta = 1
    for ( long i=m; i<np; i++ ) {
        gsl_sf_result result;
        gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
        processes_lambda[i] = 1.0/ ( result.val*delta );

        if ( result.err > 10e-3 ) {
            cout << "Error > 10e-3: " << result.err << endl;
        }

    }

    // Initial time event
    for ( long i=0; i< ( long ) processes_state.size(); i++ ) {
        if ( processes_state[i] == INFECTED ) {
            // Spreading
            long mout = i/n;
            long vout = i%n;
            for ( long j=0; j< ( long ) M.IL[mout][vout].size(); j++ ) {

                long id = M.IL[mout][vout][j];
                double rnd = 0;

                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                } while ( rnd == 0 );

                pair<long, double> process = make_pair ( id, rnd );
                processes_time.push_back ( process );

            }

            // Recovery
            long id = m+i;
            double rnd = 0;
            do {
                rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
            } while ( rnd == 0 );

            pair<long, double> process = make_pair ( id, rnd );
            processes_time.push_back ( process );
        }
    }

    // QS method
    vector <double> barP ( M.M*M.N, 0 );
    vector <vector <char> > QS_list ( QSM );
    vector < long > QS_Ni ( QSM );

    vector <vector <long> > QS_mData_i ( QSM ), QS_mData_s ( QSM );

    for ( long i=0; i<QSM; i++ ) {
        QS_list[i] = vector <char> ( processes_state );

        QS_Ni[i] = Ni;

        QS_mData_i[i] = vector <long> ( mData_i );
        QS_mData_s[i] = vector <long> ( mData_s );
    }
    // QS method


    // Iterate the dynamics 
    // adaptative time control
    double X_past = 0;
    long k_QSta = 1;
    int flag = 0;
    double t = 0;
    double Deltat = 0;

    time ( &tic );
    
    // Iterate the dynamics 
    while ( flag == 0 ) {

        // Next process

        double min = processes_time[0].second;
        long min_pos = 0;
        for ( long i=1; i< ( long ) processes_time.size(); i++ ) {
            if ( min > processes_time[i].second ) {
                min = processes_time[i].second;
                min_pos = i;
            }
        }
        long id_process = processes_time[min_pos].first;
        Deltat = min-t;
        t = min;
        processes_time.erase ( processes_time.begin()+min_pos );


        if ( t > QStr ) {
            if ( Ni > 0 ) {
                barP[Ni] += Deltat;

                for ( long k=0; k<l; k++ ) {
                    barPm[k][mData_i[k]] += Deltat;

               }
            }
        }

        // Prerform the dynamical rules
        if ( id_process < m ) {
            // Contact process
            long to = M.EL[id_process].first.first;
            long from = M.EL[id_process].second.first;

            long to_l = M.EL[id_process].first.second;
            long from_l = M.EL[id_process].second.second;

            if ( processes_state[from+n*from_l] != INFECTED ) {
                cout << "Erro!!!!!!!" << endl;
                exit ( 1 );
            }

            if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == SUSCEPTIBLE ) {
                processes_state[to+n*to_l] = INFECTED;
                Ni++;

                mData_i[to_l]++;
                mData_s[to_l]--;

                // Next event on the process -- begin
                // Spreading
                for ( long j=0; j< ( long ) M.IL[to_l][to].size(); j++ ) {

                    long id = M.IL[to_l][to][j];
                    double rnd = 0;
                    do {
                        rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                    } while ( rnd == 0 );

                    pair<long, double> node_push;
                    node_push.first  = id;
                    node_push.second = t + rnd;
                    processes_time.push_back ( node_push );
                }


                // Spreading of the same process -- new time
                double rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id_process], alpha );
                } while ( rnd == 0 );

                pair<long, double> node_push;
                node_push.first  = id_process;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );

                // Recovery
                long id = m+ ( to+n*to_l );
                rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                } while ( rnd == 0 );


                node_push.first  = id;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );
                // Next event on the process -- end

            } else if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == INFECTED ) {
                // Spreading of the same process
                double rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id_process], alpha );
                } while ( rnd == 0 );

                pair<long, double> node_push;
                node_push.first  = id_process;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );
            }

        } else {
            // Spontaneous process
            if ( processes_state[id_process-m] == INFECTED ) {
                processes_state[id_process-m] = SUSCEPTIBLE;
                Ni--;

                long layer = ( id_process-m ) /n;
                long node = ( id_process-m ) %n;

                mData_i[layer]--;
                mData_s[layer]++;

                // Next event on the process
                for ( long i= ( long ) processes_time.size()-1; i>=0; i-- ) {
                    long id = processes_time[i].first;

                    if ( id < m ) {
                        if ( M.EL[id].second.first == node && M.EL[id].second.second == layer ) {
                            processes_time.erase ( processes_time.begin()+i );
                        }
                    }
                }

            }

        }

        // QS methods
        if ( Ni == 0 ) {
            long index = ( long ) gsl_rng_uniform_int ( randomic, QSM );

            processes_state = vector <char> ( QS_list[index] );
            Ni = QS_Ni[index];

            // Reset time event
            processes_time.erase ( processes_time.begin(), processes_time.end() );

            for ( long i=0; i< ( long ) processes_state.size(); i++ ) {
                if ( processes_state[i] == INFECTED ) {
                    // Spreading
                    long mout = i/n;
                    long vout = i%n;
                    for ( long j=0; j< ( long ) M.IL[mout][vout].size(); j++ ) {

                        long id = M.IL[mout][vout][j];
                        double rnd = 0;

                        do {
                            rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                        } while ( rnd == 0 );

                        pair<long, double> process = make_pair ( id, t+rnd );
                        processes_time.push_back ( process );

                    }

                    // Recovery
                    long id = m+i;
                    double rnd = 0;
                    do {
                        rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                    } while ( rnd == 0 );

                    pair<long, double> process = make_pair ( id, t+rnd );
                    processes_time.push_back ( process );
                }
            }

            mData_i = vector <long> ( QS_mData_i[index] );
            mData_s = vector <long> ( QS_mData_s[index] );


        } else if ( gsl_ran_exponential ( randomic, 1.0/QSpr ) < Deltat ) {

            long index = ( long ) gsl_rng_uniform_int ( randomic, QSM );
            QS_list.erase ( QS_list.begin()+index );
            QS_list.insert ( QS_list.begin(), processes_state );

            QS_Ni.erase ( QS_Ni.begin()+index );
            QS_Ni.insert ( QS_Ni.begin(), Ni );

            QS_mData_i.erase ( QS_mData_i.begin()+index );
            QS_mData_i.insert ( QS_mData_i.begin(), mData_i );

            QS_mData_s.erase ( QS_mData_s.begin()+index );
            QS_mData_s.insert ( QS_mData_s.begin(), mData_s );
        }

        // adaptative time control -- begin
        if ( t >= 2*QStr + k_QSta*QSta ) {
            k_QSta++;
            double m1 = 0.0, m2 = 0.0, summation = 0.0;
            for ( long i=1; i< ( long ) barP.size(); i++ ) {
                summation += barP[i];
            }

            for ( long i=1; i< ( long ) barP.size(); i++ ) {
                m1 += ( barP[i]/summation ) *i;
                m2 += ( barP[i]/summation ) *i*i;

            }

            double X_new = ( ( m2 - m1*m1 ) / ( m1 ) );
            double X_diff = fabs ( X_new - X_past );


            for ( long k=0; k<l; k++ ) {
                m1 = 0.0;
                m2 = 0.0;
                summation = 0.0;

                for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
                    summation += barPm[k][i];
                }

                for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
                    m1 += ( barPm[k][i]/summation ) *i;
                    m2 += ( barPm[k][i]/summation ) *i*i;
                }

                X2[k] = ( ( m2 - m1*m1 ) / ( m1 ) );
                rho2[k] = m1;
            }


            if ( X_diff < 0.0001 || t >= 2*QStr + 100*QSta ) {
                printf ( "ENDING: lambda = %.6lf||eta = %.6lf||X_new = %lf||X_diff = %.6lf||", lambda, eta, X_new, X_diff );

                for ( long k=0; k<M.M; k++ ) {
                    printf ( "rho[%ld] = %.6lf||X[%ld] = %.6lf||", k, rho2[k], k, X2[k] );
                    fflush ( stdout );
                }
                cout << endl;

                flag = 1;

            } else {
                printf ( "CONTINUING: lambda = %.6lf||eta = %.6lf||X_new = %lf||X_diff = %.6lf|| ", lambda, eta, X_new, X_diff );

                for ( long k=0; k<M.M; k++ ) {
                    printf ( "rho[%ld] = %.6lf||X[%ld] = %.6lf||", k, rho2[k], k, X2[k] );
                    fflush ( stdout );
                }
                time ( &toc );
                printf ( "(t = %.6e )--[t_run = %.6e]\n", t, difftime ( toc, tic ) );
                time ( &tic );

            }

            X_past = X_new;

        }
        // adaptative time control -- end

    }

    double m1 = 0.0, m2 = 0.0, summation = 0.0;
    for ( long i=1; i< ( long ) barP.size(); i++ ) {
        summation += barP[i];
    }

    for ( long i=1; i< ( long ) barP.size(); i++ ) {
        m1 += ( barP[i]/summation ) *i;
        m2 += ( barP[i]/summation ) *i*i;

    }

    X = ( ( m2 - m1*m1 ) / ( m1 ) );
    rho = m1;

    for ( long k=0; k<l; k++ ) {
        m1 = 0.0;
        m2 = 0.0;
        summation = 0.0;

        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            summation += barPm[k][i];
        }

        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            m1 += ( barPm[k][i]/summation ) *i;
            m2 += ( barPm[k][i]/summation ) *i*i;
        }

        X2[k] = ( ( m2 - m1*m1 ) / ( m1 ) );
        rho2[k] = m1;
    }

    // writing probability densities
    // QS P -- Writing data
    char QS_fname[256];
    sprintf ( QS_fname, "Pm_lambda_%.6lf_eta_%.6lf_%s", lambda, eta, fname.data() );
    FILE *QS_output = fopen ( QS_fname, "w" );
    for ( long i=0; i< ( long ) barP.size(); i++ ) {
        fprintf ( QS_output, "%.6e\t", barP[i] );
    }
    fprintf ( QS_output, "\n" );

    for ( long k=0; k<l; k++ ) {
        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            fprintf ( QS_output, "%.6e\t", barPm[k][i] );
        }
        fprintf ( QS_output, "\n" );
    }


    fclose ( QS_output );

    return 1;
}


// Contact Process
int QS_Adaptative_GCP ( Multilayer &M, double i0, double alpha, double lambda, double eta, double delta, double QSpr, double QStr, double QSta, long QSM, gsl_rng * randomic, double &X, double &rho, vector<double> &X2, vector<double> &rho2, string fname )
{
    // time
    time_t tic, toc;

    // Multilayer properties
    long n = M.N;
    long l = M.M;
    long m = M.E;

    // Data
    long np = n*l+m;

    // Auxiliary variables
    long Ni = 0;

    // Layer statistics
    vector <vector <double> > barPm ( M.M );
    vector <long> mData_i ( l ), mData_s ( l );
    for ( long k=0; k<l; k++ ) {
        mData_i[k] = 0;
        mData_s[k] = 0;

        barPm[k] = vector <double> ( M.N,0 );
    }

    // Epidemic initialization
    vector <char> processes_state ( n*l );

    do {
        Ni = 0;
        for ( long k=0; k<l; k++ ) {
            for ( long i=0; i<n; i++ ) {
                if ( gsl_rng_uniform ( randomic ) < i0 ) {
                    processes_state[i+k*n] = INFECTED;

                    mData_i[k]++;

                    Ni++;

                } else {
                    processes_state[i+k*n] = SUSCEPTIBLE;

                    mData_s[k]++;
                }
            }
        }
    } while ( Ni == 0 );


    // Processes initialization
    vector < pair<long, double> > processes_time ( 0 );
    vector <double> processes_lambda ( np );

    // Lambda rate
    for ( long i=0; i<m; i++ ) {
        long node = M.EL[i].second.first;
        long layer = M.EL[i].second.second;
        double k_node = ( ( double ) M.kInter[layer][node] ) + ( ( double ) M.kIntra[layer][node] );

        if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
            gsl_sf_result result;
            gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
            processes_lambda[i] = 1.0/ ( result.val* ( lambda/k_node ) ); // MUDEI AQUI!!!!!!

            if ( result.err > 10e-3 ) {
                cout << "Error > 10e-3: "<< result.err  << endl;
            }


        } else {
            gsl_sf_result result;
            gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
            processes_lambda[i] = 1.0/ ( result.val* ( eta/k_node ) ); // MUDEI AQUI!!!!!!

            if ( result.err > 10e-3 ) {
                cout << "Error > 10e-3: " << result.err << endl;
            }

        }
    }

    // Delta
    for ( long i=m; i<np; i++ ) {
        gsl_sf_result result;
        gsl_sf_gamma_e ( ( 1+1.0/alpha ), &result );
        processes_lambda[i] = 1.0/ ( result.val*delta );

        if ( result.err > 10e-3 ) {
            cout << "Error > 10e-3: " << result.err << endl;
        }

    }

    // Initial time event
    for ( long i=0; i< ( long ) processes_state.size(); i++ ) {
        if ( processes_state[i] == INFECTED ) {
            // Spreading
            long mout = i/n;
            long vout = i%n;
            for ( long j=0; j< ( long ) M.IL[mout][vout].size(); j++ ) {

                long id = M.IL[mout][vout][j];
                double rnd = 0;

                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                } while ( rnd == 0 );

                pair<long, double> process = make_pair ( id, rnd );
                processes_time.push_back ( process );

            }

            // Recovery
            long id = m+i;
            double rnd = 0;
            do {
                rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
            } while ( rnd == 0 );

            pair<long, double> process = make_pair ( id, rnd );
            processes_time.push_back ( process );
        }
    }

    // QS method
    vector <double> barP ( M.M*M.N, 0 );
    vector <vector <char> > QS_list ( QSM );
    vector < long > QS_Ni ( QSM );

    vector <vector <long> > QS_mData_i ( QSM ), QS_mData_s ( QSM );

    for ( long i=0; i<QSM; i++ ) {
        QS_list[i] = vector <char> ( processes_state );

        QS_Ni[i] = Ni;

        QS_mData_i[i] = vector <long> ( mData_i );
        QS_mData_s[i] = vector <long> ( mData_s );
    }
    // QS method


    // Iterate the dynamics
    // adaptative time control
    double X_past = 0;
    long k_QSta = 1;
    int flag = 0;
    double t = 0;
    double Deltat = 0;

    time ( &tic );
    
    // Iterate the dynamics
    while ( flag == 0 ) {

        // Next process

        double min = processes_time[0].second;
        long min_pos = 0;
        for ( long i=1; i< ( long ) processes_time.size(); i++ ) {
            if ( min > processes_time[i].second ) {
                min = processes_time[i].second;
                min_pos = i;
            }
        }
        long id_process = processes_time[min_pos].first;
        Deltat = min-t;
        t = min;
        processes_time.erase ( processes_time.begin()+min_pos );


        if ( t > QStr ) {
            if ( Ni > 0 ) {
                barP[Ni] += Deltat;

                for ( long k=0; k<l; k++ ) {
                    barPm[k][mData_i[k]] += Deltat;

                }
            }
        }

        // Prerform the dynamical rules
        if ( id_process < m ) {
            // Contact process
            long to = M.EL[id_process].first.first;
            long from = M.EL[id_process].second.first;

            long to_l = M.EL[id_process].first.second;
            long from_l = M.EL[id_process].second.second;

            if ( processes_state[from+n*from_l] != INFECTED ) {
                cout << "Erro!!!!!!!" << endl;
                exit ( 1 );
            }

            if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == SUSCEPTIBLE ) {
                processes_state[to+n*to_l] = INFECTED;
                Ni++;

                mData_i[to_l]++;
                mData_s[to_l]--;

                // Next event on the process -- begin
                // Spreading
                for ( long j=0; j< ( long ) M.IL[to_l][to].size(); j++ ) {

                    long id = M.IL[to_l][to][j];
                    double rnd = 0;
                    do {
                        rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                    } while ( rnd == 0 );

                    pair<long, double> node_push;
                    node_push.first  = id;
                    node_push.second = t + rnd;
                    processes_time.push_back ( node_push );
                }


                // Spreading of the same process
                double rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id_process], alpha );
                } while ( rnd == 0 );

                pair<long, double> node_push;
                node_push.first  = id_process;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );

                // Recovery
                long id = m+ ( to+n*to_l );
                rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                } while ( rnd == 0 );


                node_push.first  = id;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );
                // Next event on the process

            } else if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == INFECTED ) {
                // Spreading of the same process 
                double rnd = 0;
                do {
                    rnd  = gsl_ran_weibull ( randomic, processes_lambda[id_process], alpha );
                } while ( rnd == 0 );

                pair<long, double> node_push;
                node_push.first  = id_process;
                node_push.second = t + rnd;
                processes_time.push_back ( node_push );
            }

        } else {
            // Spontaneous process
            if ( processes_state[id_process-m] == INFECTED ) {
                processes_state[id_process-m] = SUSCEPTIBLE;
                Ni--;

                long layer = ( id_process-m ) /n;
                long node = ( id_process-m ) %n;

                mData_i[layer]--;
                mData_s[layer]++;

                // Next event on the process
                for ( long i= ( long ) processes_time.size()-1; i>=0; i-- ) {
                    long id = processes_time[i].first;

                    if ( id < m ) {
                        if ( M.EL[id].second.first == node && M.EL[id].second.second == layer ) {
                            processes_time.erase ( processes_time.begin()+i );
                        }
                    }
                }
                // Next event on the process

            }

        }

        // QS methods
        if ( Ni == 0 ) {
            long index = ( long ) gsl_rng_uniform_int ( randomic, QSM );

            processes_state = vector <char> ( QS_list[index] );
            Ni = QS_Ni[index];

            // Reset time event
            // new time -- begin
            processes_time.erase ( processes_time.begin(), processes_time.end() );

            for ( long i=0; i< ( long ) processes_state.size(); i++ ) {
                if ( processes_state[i] == INFECTED ) {
                    // Spreading
                    long mout = i/n;
                    long vout = i%n;
                    for ( long j=0; j< ( long ) M.IL[mout][vout].size(); j++ ) {

                        long id = M.IL[mout][vout][j];
                        double rnd = 0;

                        do {
                            rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                        } while ( rnd == 0 );

                        pair<long, double> process = make_pair ( id, t+rnd );
                        processes_time.push_back ( process );

                    }

                    // Recovery
                    long id = m+i;
                    double rnd = 0;
                    do {
                        rnd  = gsl_ran_weibull ( randomic, processes_lambda[id], alpha );
                    } while ( rnd == 0 );

                    pair<long, double> process = make_pair ( id, t+rnd );
                    processes_time.push_back ( process );
                }
            }
            // new time -- end

            mData_i = vector <long> ( QS_mData_i[index] );
            mData_s = vector <long> ( QS_mData_s[index] );


        } else if ( gsl_ran_exponential ( randomic, 1.0/QSpr ) < Deltat ) {

            long index = ( long ) gsl_rng_uniform_int ( randomic, QSM );
            QS_list.erase ( QS_list.begin()+index );
            QS_list.insert ( QS_list.begin(), processes_state );

            QS_Ni.erase ( QS_Ni.begin()+index );
            QS_Ni.insert ( QS_Ni.begin(), Ni );

            QS_mData_i.erase ( QS_mData_i.begin()+index );
            QS_mData_i.insert ( QS_mData_i.begin(), mData_i );

            QS_mData_s.erase ( QS_mData_s.begin()+index );
            QS_mData_s.insert ( QS_mData_s.begin(), mData_s );
        }

        // adaptative time control 
        if ( t >= 2*QStr + k_QSta*QSta ) {
            k_QSta++;
            double m1 = 0.0, m2 = 0.0, summation = 0.0;
            for ( long i=1; i< ( long ) barP.size(); i++ ) {
                summation += barP[i];
            }

            for ( long i=1; i< ( long ) barP.size(); i++ ) {
                m1 += ( barP[i]/summation ) *i;
                m2 += ( barP[i]/summation ) *i*i;

            }

            double X_new = ( ( m2 - m1*m1 ) / ( m1 ) );
            double X_diff = fabs ( X_new - X_past );


            for ( long k=0; k<l; k++ ) {
                m1 = 0.0;
                m2 = 0.0;
                summation = 0.0;

                for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
                    summation += barPm[k][i];
                }

                for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
                    m1 += ( barPm[k][i]/summation ) *i;
                    m2 += ( barPm[k][i]/summation ) *i*i;
                }

                X2[k] = ( ( m2 - m1*m1 ) / ( m1 ) );
                rho2[k] = m1;
            }


            if ( X_diff < 0.0001 || t >= 2*QStr + 100*QSta ) {
                printf ( "ENDING: lambda = %.6lf||eta = %.6lf||X_new = %lf||X_diff = %.6lf||", lambda, eta, X_new, X_diff );

                for ( long k=0; k<M.M; k++ ) {
                    printf ( "rho[%ld] = %.6lf||X[%ld] = %.6lf||", k, rho2[k], k, X2[k] );
                    fflush ( stdout );
                }
                cout << endl;

                flag = 1;

            } else {
                printf ( "CONTINUING: lambda = %.6lf||eta = %.6lf||X_new = %lf||X_diff = %.6lf||", lambda, eta, X_new, X_diff );

                for ( long k=0; k<M.M; k++ ) {
                    printf ( "rho[%ld] = %.6lf||X[%ld] = %.6lf||", k, rho2[k], k, X2[k] );
                    fflush ( stdout );
                }
                time ( &toc );
                printf ( "(t = %.6e )--[t_run = %.6e]\n", t, difftime ( toc, tic ) );
                time ( &tic );

            }

            X_past = X_new;

        }
        // adaptative time control 

    }

    double m1 = 0.0, m2 = 0.0, summation = 0.0;
    for ( long i=1; i< ( long ) barP.size(); i++ ) {
        summation += barP[i];
    }

    for ( long i=1; i< ( long ) barP.size(); i++ ) {
        m1 += ( barP[i]/summation ) *i;
        m2 += ( barP[i]/summation ) *i*i;

    }

    X = ( ( m2 - m1*m1 ) / ( m1 ) );
    rho = m1;

    for ( long k=0; k<l; k++ ) {
        m1 = 0.0;
        m2 = 0.0;
        summation = 0.0;

        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            summation += barPm[k][i];
        }

        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            m1 += ( barPm[k][i]/summation ) *i;
            m2 += ( barPm[k][i]/summation ) *i*i;
        }

        X2[k] = ( ( m2 - m1*m1 ) / ( m1 ) );
        rho2[k] = m1;
    }

    // writing probability densities
    // QS P -- Writing data
    char QS_fname[256];
    sprintf ( QS_fname, "Pm_lambda_%.6lf_eta_%.6lf_%s", lambda, eta, fname.data() );
    FILE *QS_output = fopen ( QS_fname, "w" );
    for ( long i=0; i< ( long ) barP.size(); i++ ) {
        fprintf ( QS_output, "%.6e\t", barP[i] );
    }
    fprintf ( QS_output, "\n" );

    for ( long k=0; k<l; k++ ) {
        for ( long i=0; i< ( long ) barPm[k].size(); i++ ) {
            fprintf ( QS_output, "%.6e\t", barPm[k][i] );
        }
        fprintf ( QS_output, "\n" );
    }


    fclose ( QS_output );

    return 1;
}
