cmake ./
make

cd ./Examples/
gunzip -c Examples.tar.gz | tar xvf -
cp ../spreadingmultilayer ./Data_MK/
cp ../spreadingmultilayer ./Data_SIS/
cp ../spreadingmultilayer ./Data_SIR/
cp ../spreadingmultilayer ./DataQS_lambda_SIS/
cp ../spreadingmultilayer ./DataQS_lambda_CP/

python CTMC_Single_Run_MK.py
python CTMC_Single_Run_SIR.py
python CTMC_Single_Run_SIS.py
python QS_lambda_SIS.py
python QS_lambda_CP.py

