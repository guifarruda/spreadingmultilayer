#ifndef Help_H
#define Help_H

#include <string.h>
#include <iostream>
#include <fstream>

using namespace std;


int PrintHelp();
int PrintRefs();
int PrintExtra();

#endif
 
