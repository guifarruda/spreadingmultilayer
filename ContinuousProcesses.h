#ifndef ContinuousProcesses_H
#define ContinuousProcesses_H

#include "Defines.h"

#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>

#include <ctime>
#include <unistd.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>

#include <limits>

#include "Multilayer.h"

const double Max_double = std::numeric_limits<double>::max();


int ContinuousProcesses_MK ( const Multilayer &M, double tmax, double i0, double beta, double eta, double alpha, double nu, gsl_rng * randomic, char *fname );
int ContinuousProcesses_SIS ( const Multilayer &M, double tmax, double i0, double beta, double eta, double gamma, gsl_rng * randomic, char *fname );
int ContinuousProcesses_SIR ( const Multilayer &M, double tmax, double i0, double beta, double eta, double gamma, gsl_rng * randomic, char *fname );

#endif
