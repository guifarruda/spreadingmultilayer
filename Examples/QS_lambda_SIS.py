# This simple script exemplify one of the main results of 
#  [1] G. F. de Arruda, E. Cozzo, T. P. Peixoto, F. A. Rodrigues, and Y. Moreno, Phys. Rev. X 7, 011014 (2017).
#      https://doi.org/10.1103/PhysRevX.7.011014
# Here we show how a 2-Layer Muliplex network an present multiple susceptibility peaks.
# Furthermore, by varying \frac{\eta}{\lambda} we also show how such a peak may vanish.
# This phenomena was initialy reported in [1]. Some of the methods used in this
# example follow the methods described in:
#   [2] G. F. de Arruda, F. A. Rodrigues, and Y. Moreno, Physics Reports 756, 1 (2018).
#      https://doi.org/10.1016/j.physrep.2018.06.007
#
# WARNING: This script is time costly. Consider running the variables cmd before
# running this script.
# NOTE: Before runnig the simulations we check if the output file already exists

import numpy as np
from matplotlib import pyplot as plt  
import os.path

import matplotlib
matplotlib.use("pdf")


plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.unicode'] = True
plt.rcParams.update({'font.size': 18})

plt.ion();

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(8,10))


# Number of nodes
N = 2*1000;

# Defining the parameters
param_file_name = "1kMuxER1030.edgelist";
param_lambda = "lambda.txt";
param_eta = 0.01;
param_delta = 1.0;
param_alpha = 1.0;
param_y0 = 0.010000;
param_ID = "0";
param_QStr = 25000;
param_QSta = 100;
param_QSp = 0.01;
param_QSm = 25;

# The output format
fname = "./DataQS_lambda_SIS/QS_SIS_%s_LambdaFile_%s_eta_%f_delta_%f_alpha_%f_y0_0.010000_ID_0.txt" % (param_file_name, param_lambda, param_eta, param_delta, param_alpha);


# If the file does not exists, make a system call and run the code
if os.path.isfile( fname ) == False:
    cmd = "./spreadingmultilayer -network " + param_file_name + " -delta " + str(param_delta) + " -eta " + str(param_eta) + " -y0 " + str(param_delta) + " -alpha " + str(param_alpha) + " -QStr " + str(param_alpha) + " -QSta " + str(param_QSta) + " -QSp " + str(param_QSp) + " -QSm " + str(param_QSm) + " -Time CTMC -Process SIS -Method QS lambda -parameters " + param_lambda;
    
    os.system(cmd);
    
# Reading Plotting data
Data = np.loadtxt( fname , delimiter='\t'); 
d = Data[:,0];
idd = d.argsort();
Data = Data[idd,:];
    
ax1.loglog(Data[:,0], Data[:,1]/N, '-or', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 0.01$');
ax2.semilogx(Data[:,0], Data[:,2]/N, '-or', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 0.01$');


# Changing \eta
param_eta = 1.0;

# The output format
fname = "./DataQS_lambda_SIS/QS_SIS_%s_LambdaFile_%s_eta_%f_delta_%f_alpha_%f_y0_0.010000_ID_0.txt" % (param_file_name, param_lambda, param_eta, param_delta, param_alpha);


# If the file does not exists, make a system call and run the code
if os.path.isfile( fname ) == False:
    cmd = "./spreadingmultilayer -network " + param_file_name + " -delta " + str(param_delta) + " -eta " + str(param_eta) + " -y0 " + str(param_delta) + " -alpha " + str(param_alpha) + " -QStr " + str(param_alpha) + " -QSta " + str(param_QSta) + " -QSp " + str(param_QSp) + " -QSm " + str(param_QSm) + " -Time CTMC -Process SIS -Method QS lambda -parameters " + param_lambda;
    
    os.system(cmd);
    
# Reading Plotting data
Data = np.loadtxt( fname , delimiter='\t'); 
d = Data[:,0];
idd = d.argsort();
Data = Data[idd,:];
    
ax1.loglog(Data[:,0], Data[:,1]/N, '-sb', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 1.0$');
ax2.semilogx(Data[:,0], Data[:,2]/N, '-sb', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 1.0$');


# Changing \eta
param_eta = 20.0;

# The output format
fname = "./DataQS_lambda_SIS/QS_SIS_%s_LambdaFile_%s_eta_%f_delta_%f_alpha_%f_y0_0.010000_ID_0.txt" % (param_file_name, param_lambda, param_eta, param_delta, param_alpha);


# If the file does not exists, make a system call and run the code
if os.path.isfile( fname ) == False:
    cmd = "./spreadingmultilayer -network " + param_file_name + " -delta " + str(param_delta) + " -eta " + str(param_eta) + " -y0 " + str(param_delta) + " -alpha " + str(param_alpha) + " -QStr " + str(param_alpha) + " -QSta " + str(param_QSta) + " -QSp " + str(param_QSp) + " -QSm " + str(param_QSm) + " -Time CTMC -Process SIS -Method QS lambda -parameters " + param_lambda;
    
    os.system(cmd);
    
# Reading Plotting data
Data = np.loadtxt( fname , delimiter='\t'); 
d = Data[:,0];
idd = d.argsort();
Data = Data[idd,:];
    
ax1.loglog(Data[:,0], Data[:,1]/N, '-vg', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 20.0$');
ax2.semilogx(Data[:,0], Data[:,2]/N, '-vg', alpha=0.45, label=r'$\frac{\eta}{\lambda} = 20.0$');


ax1.set_ylabel(r'$\chi$')

ax2.set_ylabel(r'$\rho$')
ax2.set_xlabel(r'$\lambda$')


ax2.legend(loc=2, fancybox=False, edgecolor='k', fontsize=24)

plt.tight_layout();

plt.savefig("./Example_QS_lambda_SIS.pdf");
