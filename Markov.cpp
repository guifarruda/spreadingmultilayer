#include "Markov.h"

double activity ( const Multilayer &M, long i,  int layer,  double gamma )
{
    if ( gamma >= 1e4 ) {
        return 1;
    } else {
        double k = ( ( double ) M.kIntra[layer][i] );

        return ( 1 - pow ( ( 1 -1.0/k ), gamma ) );
    }

}

int Markov_SIS_PostProcessing ( const Multilayer &Ml, long Steps, double **p, FILE *fpf, FILE *fp_out )
{
    long N = ( long ) Ml.N;
    long M = ( long ) Ml.M;


    double mean_p = 0;
    for ( long i=0; i<N*M; i++ ) {
        mean_p += p[i][Steps-1];

        fprintf ( fpf, "%e\n", p[i][Steps-1] );


    }

    double *mean = new double[M];
    for ( long j=0; j<M; j++ ) {
        mean[j] = 0;

        for ( long i=0; i<N; i++ ) {
            mean[j] += p[i+N*j][Steps-1];
        }
    }

    printf ( "rho_y = %e\trho_z = %e\trho_x = %e\t", mean_p / ( N*M ), 0.0,  1.0 - (mean_p) / ( N*M ) );
    for ( long j=0; j<M; j++ ) {
        printf ( "rho_y[%ld] = %e\trho_z[%ld] = %e\trho_x[%ld] = %e\t", j, mean[j]/N, j, 0.0, j, 1.0 - (mean[j])/N );
    }
    printf ( "\n" );
    fflush ( stdout );

    for ( long i=0; i<N*M; i++ ) {
        delete[] p[i];
        
    }

    delete[] p;
    delete[] mean;

    fclose ( fpf );
    fclose ( fp_out );

    return 0;

}

int Markov_SIR_PostProcessing ( const Multilayer &Ml, long Steps, sir p_sir, FILE *fpf, FILE *fp_out )
{
    long N = ( long ) Ml.N;
    long M = ( long ) Ml.M;

    double** p = p_sir.p;
    double** r = p_sir.r;

    double mean_p = 0;
    double mean_r = 0;
    for ( long i=0; i<N*M; i++ ) {
        mean_p += p[i][Steps-1];

        mean_r += r[i][Steps-1];

        fprintf ( fpf, "%e\t%e\n", p[i][Steps-1], r[i][Steps-1] );


    }

    double *mean = new double[M];
    double *meanr = new double[M];
    for ( long j=0; j<M; j++ ) {
        mean[j] = 0;
        meanr[j] = 0;

        for ( long i=0; i<N; i++ ) {
            mean[j] += p[i+N*j][Steps-1];

            meanr[j] += r[i+N*j][Steps-1];
        }
    }

    fprintf ( fp_out, "%e\t%e\t%e\t", mean_p / ( N*M ), mean_r / ( N*M ),  1.0 - (mean_p + mean_r) / ( N*M ) );
    for ( long j=0; j<M; j++ ) {
        fprintf ( fp_out, "%e\t%e\t%e\t", mean[j]/N, meanr[j]/N, 1.0 - (mean[j] + meanr[j])/N );
    }
    fprintf ( fp_out, "\n" );


    printf ( "rho_y = %e\trho_z = %e\trho_x = %e\t", mean_p / ( N*M ), mean_r / ( N*M ),  1.0 - (mean_p + mean_r) / ( N*M ) );
    for ( long j=0; j<M; j++ ) {
        printf ( "rho_y[%ld] = %e\trho_z[%ld] = %e\trho_x[%ld] = %e\t", j, mean[j]/N, j, meanr[j]/N, j, 1.0 - (mean[j] + meanr[j])/N );
    }
    printf ( "\n" );
    fflush ( stdout );

    for ( long i=0; i<N*M; i++ ) {
        delete[] p[i];

        delete[] r[i];
    }

    delete[] p;
    delete[] mean;

    delete[] r;
    delete[] meanr;

    fclose ( fpf );
    fclose ( fp_out );

    return 0;

}

double** Markov_SIS ( const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma )
{
    long N = ( long ) Ml.N;
    long M = ( long ) Ml.M;

    double **p = new double*[N*M];

    for ( long i=0; i<N*M; i++ ) {
        p[i] = new double[Steps];
        p[i][0] = p0;
    }

    for ( long t=0; t<Steps; t++ ) {
        double *q = new double[N*M];

        for ( long i=0; i<N*M; i++ ) {
            q[i] = 1.0;
        }

        // interlayer relationships
        #pragma omp parallel for
        for ( long k=0; k<M; k++ ) {
            for ( long i=0; i<N; i++ ) {
                long j = -1;
                long lj = -1;
                for ( long a=0; a< ( long ) Ml.ML[k][i].size(); a++ ) {
                    j = ( long ) Ml.ML[k][i][a].first;
                    lj = ( long ) Ml.ML[k][i][a].second;

                    if ( lj == k ) {
                        // interlayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - lambda * activity ( Ml, j, lj, gamma ) * p[j+N*lj][t] );

                    } else {
                        // intralayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - eta * p[j+N*lj][t] );

                    }
                }
            }
        }

        // Evolution of the system!
        #pragma omp parallel for
        for ( long i=0; i<N*M; i++ ) {
            p[i][t+1] = ( 1 - p[i][t] ) * ( 1 - q[i] ) + ( 1 - delta ) * p[i][t];
        }

        delete[] q;
    }

    return p;
}


double** Markov_SIS_Reinfection ( const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma )
{
    long N = ( long ) Ml.N;
    long M = ( long ) Ml.M;

    double **p = new double*[N*M];

    for ( long i=0; i<N*M; i++ ) {
        p[i] = new double[Steps];
        p[i][0] = p0;
    }

    for ( long t=0; t<Steps-1; t++ ) {
        double *q = new double[N*M];

        for ( long i=0; i<N*M; i++ ) {
            q[i] = 1.0;
        }

        // interlayer relationships
        #pragma omp parallel for
        for ( long k=0; k<M; k++ ) {
            for ( long i=0; i<N; i++ ) {
                long j = -1;
                long lj = -1;
                for ( long a=0; a< ( long ) Ml.ML[k][i].size(); a++ ) {
                    j = ( long ) Ml.ML[k][i][a].first;
                    lj = ( long ) Ml.ML[k][i][a].second;

                    if ( lj == k ) {
                        // interlayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - lambda * activity ( Ml, j, lj, gamma ) * p[j+N*lj][t] );

                    } else {
                        // intralayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - eta * p[j+N*lj][t] );

                    }
                }
            }
        }

        // Evolution of the system!
        #pragma omp parallel for
        for ( long i=0; i<N*M; i++ ) {
            p[i][t+1] = ( 1 - p[i][t] ) * ( 1 - q[i] ) + ( 1 - delta ) * p[i][t] + delta * ( 1 - q[i] ) * p[i][t];
        }

        delete[] q;
    }

    return p;
}


sir Markov_SIR ( const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma )
{
    long N = ( long ) Ml.N;
    long M = ( long ) Ml.M;

    sir p;
    p.p = new double*[N*M];
    p.r = new double*[N*M];

    for ( long i=0; i<N*M; i++ ) {
        p.p[i] = new double[Steps];
        p.p[i][0] = p0;

        p.r[i] = new double[Steps];
        p.r[i][0] = 0.0;
    }

    //p[0][0] = 1;

    for ( long t=0; t<Steps; t++ ) {
        double *q = new double[N*M];

        for ( long i=0; i<N*M; i++ ) {
            q[i] = 1.0;
        }

        // interlayer relationships
        #pragma omp parallel for
        for ( long k=0; k<M; k++ ) {
            for ( long i=0; i<N; i++ ) {
                long j = -1;
                long lj = -1;
                for ( long a=0; a< ( long ) Ml.ML[k][i].size(); a++ ) {
                    j = ( long ) Ml.ML[k][i][a].first;
                    lj = ( long ) Ml.ML[k][i][a].second;

                    if ( lj == k ) {
                        // interlayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - lambda * activity ( Ml, j, lj, gamma ) * p.p[j+N*lj][t] );

                    } else {
                        // intralayer relationships
                        q[i+N*k] = q[i+N*k] * ( 1 - eta * p.p[j+N*lj][t] );

                    }
                }
            }
        }

        // Evolution of the system!
        #pragma omp parallel for
        for ( long i=0; i<N*M; i++ ) {
            p.p[i][t+1] = ( 1.0 - p.p[i][t] - p.r[i][t] ) * ( 1.0 - q[i] ) + ( 1.0 - delta ) * p.p[i][t];
            p.r[i][t+1] = p.r[i][t] + delta * p.p[i][t];
        }

        delete[] q;
    }

    return p;
}
