#include "ContinuousProcesses.h"


int Write_State ( vector <char> processes_state, string fname )
{
     FILE *fp = fopen ( fname.data(), "w" );

     for ( long i=0; i< ( long ) processes_state.size(); i++ ) {
          fprintf ( fp, "%ld\n", ( long ) processes_state[i] );
     }

     fclose ( fp );

     return 0;
}

int ContinuousProcesses_MK ( const Multilayer &M, double tmax, double i0, double beta, double eta, double alpha, double nu, gsl_rng * randomic, char *fname )
{
     // Multilayer properties
     long n = M.N;
     long m = M.E;
     long l = M.M;

     // Auxiliary variables
     long Ni = 0, Ns = 0, Nr = 0;

     // Data
     long np = 2*m;
     const long max_it = 10000*np;
     const long max_buffer = 10*np;
     long buffer_it = 0;

     vector <long> Data_i ( max_buffer ), Data_s ( max_buffer ), Data_r ( max_buffer );
     vector <double> Data_t ( max_buffer );

     // Layer statistics
     vector <long> mData_i ( l ), mData_s ( l ), mData_r ( l );
     vector < vector <long> > mbData_i ( l ), mbData_s ( l ), mbData_r ( l );
     for ( long k=0; k<l; k++ ) {
          mbData_i[k].resize ( max_buffer );
          mbData_s[k].resize ( max_buffer );
          mbData_r[k].resize ( max_buffer );

          mData_i[k] = 0;
          mData_s[k] = 0;
          mData_r[k] = 0;
     }

     // Poisson processes initialization
     vector <double> processes_time ( np );
     vector <double> processes_lambda ( np );

     // Lambda rate
     for ( long i=0; i<m; i++ ) {
          if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
               processes_lambda[i] = beta;
          } else {
               processes_lambda[i] = eta;
          }
     }

     for ( long i=0; i<m; i++ ) {
          if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
               processes_lambda[i+m] = alpha;
          } else {
               processes_lambda[i+m] = nu;
          }
     }

     // Epidemic initialization
     vector <char> processes_state ( n*l );
     for ( long k=0; k<l; k++ ) {
          for ( long i=0; i<n; i++ ) {
               if ( gsl_rng_uniform ( randomic ) < i0 ) {
                    processes_state[i+k*n] = SPREADER;
                    Ni++;

                    mData_i[k]++;

               } else {
                    processes_state[i+k*n] = IGNORANT;
                    Ns++;

                    mData_s[k]++;
               }
          }
     }

     // Initial time event
     for ( long i=0; i< ( long ) processes_time.size(); i++ ) {
          // aqui

          long id_process_index = i;
          if ( i >= m ) {
               id_process_index = id_process_index - m;
          }

          long from = M.EL[id_process_index].second.first;
          long from_l = M.EL[id_process_index].second.second;

          if ( processes_state[from+n*from_l] == SPREADER ) {

               double rnd = 0;
               do {
                    rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
               } while ( rnd == 0 );

               processes_time[i] = rnd;
          } else {
               processes_time[i] = Max_double;
          }
     }


     // Writing data
     FILE *output = fopen ( fname, "w" );

     // Print info
     printf ( "n = %ld\tm = %ld\n", n, m );
     printf ( "np = %ld\n", np );
     fflush ( stdout );

     // Iterate the dynamics
     double t = 0;
     long it = 0;
     long nimin = 0;
     for ( ; ( it<max_it && Ni > nimin && t <= tmax ); it++ ) {
          // Next process
          long id_process = min_element ( processes_time.begin(), processes_time.end() ) - processes_time.begin();
          t = processes_time[id_process];

          // Prerform the dynamical rules

          // Contact process
          long id_process_index = id_process;
          if ( id_process >= m ) {
               id_process_index = id_process - m;
          }

          long to = M.EL[id_process_index].first.first;
          long from = M.EL[id_process_index].second.first;

          long to_l = M.EL[id_process_index].first.second;
          long from_l = M.EL[id_process_index].second.second;

          // Print
          if ( it%1000 == 0 ) {
               printf ( "%ld\t%.6e\t%ld\t%ld\t%ld", it, processes_time[id_process], Ns, Ni, Nr );

               for ( long k=0; k<l; k++ ) {
                    printf ( "\t%ld\t%ld\t%ld\t%ld", mData_i[k], mData_s[k], mData_r[k], ( mData_i[k]+mData_s[k]+mData_r[k] ) );
               }

               printf ( "\n" );
          }

          // Saving data
          Data_i[buffer_it] = Ni;
          Data_s[buffer_it] = Ns;
          Data_r[buffer_it] = Nr;
          Data_t[buffer_it] = processes_time[id_process];

          for ( long k=0; k<l; k++ ) {
               mbData_i[k][buffer_it] = mData_i[k];
               mbData_s[k][buffer_it] = mData_s[k];
               mbData_r[k][buffer_it] = mData_r[k];
          }

          buffer_it++;

          if ( buffer_it == max_buffer ) {
               for ( long i=0 ; i<max_buffer; i++ ) {
                    fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

                    for ( long k=0; k<l; k++ ) {
                         fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
                    }

                    fprintf ( output, "\n" );
               }
               fflush ( output );

               buffer_it = 0;
          }




          if ( processes_state[from+n*from_l] != SPREADER ) {
               printf ( "E1\n" );
          }

          if ( id_process < m && processes_state[from+n*from_l] == SPREADER && processes_state[to+n*to_l] == IGNORANT ) {

               processes_state[to+n*to_l] = SPREADER;
               Ni++;
               Ns--;

               mData_i[M.EL[id_process_index].first.second]++;
               mData_s[M.EL[id_process_index].first.second]--;

               // adding new processes
               for ( long i=0; i< ( long ) processes_time.size(); i++ ) {
                    long id_process_index_tmp = i;
                    if ( i >= m ) {
                         id_process_index_tmp = id_process_index_tmp - m;
                    }

                    long from_tmp = M.EL[id_process_index_tmp].second.first;
                    long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                    if ( from_tmp == to && from_l_tmp == to_l ) {

                         double rnd = 0;
                         do {
                              rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                         } while ( rnd == 0 );

                         processes_time[i] = t+rnd;
                    }
               }
               processes_time[id_process] += gsl_ran_exponential ( randomic, 1.0/processes_lambda[id_process] );			// rever!!! estava Max_double


          } else if ( id_process >= m && processes_state[from+n*from_l] == SPREADER && ( processes_state[to+n*to_l] == STIFLER || processes_state[to+n*to_l] == SPREADER ) ) {
               processes_state[from+n*from_l] = STIFLER;
               Ni--;
               Nr++;

               mData_r[M.EL[id_process_index].second.second]++;
               mData_i[M.EL[id_process_index].second.second]--;

               // removing processes
               for ( long i=0; i< ( long ) processes_time.size(); i++ ) {
                    long id_process_index_tmp = i;
                    if ( i >= m ) {
                         id_process_index_tmp = id_process_index_tmp - m;
                    }

                    long from_tmp = M.EL[id_process_index_tmp].second.first;
                    long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                    if ( from_tmp == from && from_l_tmp == from_l ) {
                         processes_time[i] = Max_double;
                    }
               }

          } else {
               // Next event on the Poisson process if nothing happend
               double rnd = 0;
               rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[id_process] );

               // aqui
               processes_time[id_process] += rnd;
          }

     }

     // Writing data
     for ( long i=0 ; i<buffer_it-1; i++ ) {
          fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

          for ( long k=0; k<l; k++ ) {
               fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
          }

          fprintf ( output, "\n" );
     }
     fclose ( output );


     return 1;
}


int ContinuousProcesses_SIS ( const Multilayer &M, double tmax, double i0, double beta, double eta, double gamma, gsl_rng * randomic, char *fname )
{
     // Multilayer properties
     long n = M.N;
     long m = M.E;
     long l = M.M;

     // Auxiliary variables
     long Ni = 0, Ns = 0, Nr = 0;

     // Data
     long np = n*l+m;
     const long max_it = 10000*np;
     const long max_buffer = 10*np;
     long buffer_it = 0;

     vector <long> Data_i ( max_buffer ), Data_s ( max_buffer ), Data_r ( max_buffer );
     vector <double> Data_t ( max_buffer );

     // Layer statistics
     vector <long> mData_i ( l ), mData_s ( l ), mData_r ( l );
     vector < vector <long> > mbData_i ( l ), mbData_s ( l ), mbData_r ( l );
     for ( long k=0; k<l; k++ ) {
          mbData_i[k].resize ( max_buffer );
          mbData_s[k].resize ( max_buffer );
          mbData_r[k].resize ( max_buffer );

          mData_i[k] = 0;
          mData_s[k] = 0;
          mData_r[k] = 0;
     }


     // Epidemic initialization
     vector <char> processes_state ( n*l );
     for ( long k=0; k<l; k++ ) {
          for ( long i=0; i<n; i++ ) {
               if ( gsl_rng_uniform ( randomic ) < i0 ) {
                    processes_state[i+k*n] = INFECTED;
                    Ni++;

                    mData_i[k]++;

               } else {
                    processes_state[i+k*n] = SUSCEPTIBLE;
                    Ns++;

                    mData_s[k]++;
               }
          }
     }

     // Writing data
     FILE *output = fopen ( fname, "w" );

     // Print info
     printf ( "n = %ld\tm = %ld\n", n, m );

     // Poisson processes initialization
     vector <double> processes_time ( np );
     vector <double> processes_lambda ( np );

     // Lambda rate
     for ( long i=0; i<m; i++ ) {
          if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
               processes_lambda[i] = beta;
          } else {
               processes_lambda[i] = eta;
          }
     }

     // Delta = 1
     for ( long i=m; i<np; i++ ) {
          processes_lambda[i] = gamma;
     }

     // Initial time event
     for ( long i=0; i< ( long ) processes_time.size(); i++ ) {
          // aqui

          long id_process_index = i;
          if ( i < m ) { // contact processes
               long from = M.EL[id_process_index].second.first;
               long from_l = M.EL[id_process_index].second.second;

               if ( processes_state[from+n*from_l] == INFECTED ) {

                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                    } while ( rnd == 0 );

                    processes_time[i] = rnd;
               } else {
                    processes_time[i] = Max_double;
               }

          } else { // Spontaneous processes
               if ( processes_state[id_process_index-m] == INFECTED ) {
                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                    } while ( rnd == 0 );

                    processes_time[i] = rnd;
               } else {
                    processes_time[i] = Max_double;
               }
          }
     }


     // Iterate the dynamics
     double t = 0;
     long it = 0;
     long nimin = 0;//( long ) floor ( n/1000 );
     for ( ; ( it<max_it && Ni > nimin && t <= tmax ); it++ ) {
          // Next process
          long id_process = min_element ( processes_time.begin(), processes_time.end() ) - processes_time.begin();
          t = processes_time[id_process];


          // Prerform the dynamical rules
          if ( id_process < m ) {
               // Contact process
               long to = M.EL[id_process].first.first;
               long from = M.EL[id_process].second.first;

               long to_l = M.EL[id_process].first.second;
               long from_l = M.EL[id_process].second.second;

               if ( processes_state[from+n*from_l] == SUSCEPTIBLE ) {
                    printf ( "E1\n" );
               }

               if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == SUSCEPTIBLE ) {
                    processes_state[to+n*to_l] = INFECTED;
                    Ni++;
                    Ns--;

                    mData_i[M.EL[id_process].first.second]++;
                    mData_s[M.EL[id_process].first.second]--;

                    // adding new processes
                    for ( long i=0; i< ( long ) m; i++ ) {
                         long id_process_index_tmp = i;
                         long from_tmp = M.EL[id_process_index_tmp].second.first;
                         long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                         if ( from_tmp == to && from_l_tmp == to_l ) {

                              double rnd = 0;
                              do {
                                   rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                              } while ( rnd == 0 );

                              processes_time[i] = t+rnd;
                         }
                    }

                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[m+to+n*to_l] );
                    } while ( rnd == 0 );

                    processes_time[m+to+n*to_l] = t+rnd;
                    // adding new processes

               }

               processes_time[id_process] += gsl_ran_exponential ( randomic, 1.0/processes_lambda[id_process] );			// rever!!! estava Max_double

          } else {
               // Spontaneous process
               if ( processes_state[id_process-m] == SUSCEPTIBLE ) {
                    printf ( "E2 " );
               }

               if ( processes_state[id_process-m] == INFECTED ) {
                    // SIS
                    processes_state[id_process-m] = SUSCEPTIBLE;
                    Ns++;
                    Ni--;

                    long layer = ( id_process-m ) /n;

                    mData_i[layer]--;
                    mData_s[layer]++;

                    // removing processes
                    for ( long i=0; i< ( long ) m; i++ ) {
                         long id_process_index_tmp = i;

                         long from_tmp = M.EL[id_process_index_tmp].second.first;
                         long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                         if ( from_tmp == id_process - m - n*layer && from_l_tmp == layer ) {
                              processes_time[i] = Max_double;
                         }
                    }

                    processes_time[id_process] = Max_double;

               }

          }

          // Print
          if ( it%1000 == 0 ) {
               printf ( "%ld\t%.6e\t%ld\t%ld\t%ld", it, t, Ns, Ni, Nr );

               for ( long k=0; k<l; k++ ) {
                    printf ( "\t%ld\t%ld\t%ld\t%ld", mData_i[k], mData_s[k], mData_r[k], ( mData_i[k]+mData_s[k]+mData_r[k] ) );
               }

               printf ( "\n" );
          }

          // Saving data
          Data_i[buffer_it] = Ni;
          Data_s[buffer_it] = Ns;
          Data_r[buffer_it] = Nr;
          Data_t[buffer_it] = t;

          for ( long k=0; k<l; k++ ) {
               mbData_i[k][buffer_it] = mData_i[k];
               mbData_s[k][buffer_it] = mData_s[k];
               mbData_r[k][buffer_it] = mData_r[k];
          }

          buffer_it++;

          if ( buffer_it == max_buffer ) {
               for ( long i=0 ; i<max_buffer; i++ ) {
                    fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

                    for ( long k=0; k<l; k++ ) {
                         fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
                    }

                    fprintf ( output, "\n" );
               }
               fflush ( output );

               buffer_it = 0;
          }

     }

     // Writing data
     for ( long i=0 ; i<buffer_it; i++ ) {
          fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

          for ( long k=0; k<l; k++ ) {
               fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
          }

          fprintf ( output, "\n" );
     }
     fclose ( output );

     char state_name[256];
     sprintf ( state_name, "States_%s", fname );
     Write_State ( processes_state, state_name );


     return 1;
}


int ContinuousProcesses_SIR ( const Multilayer &M, double tmax, double i0, double beta, double eta, double gamma, gsl_rng * randomic, char *fname )
{
     // Multilayer properties
     long n = M.N;
     long m = M.E;
     long l = M.M;

     // Auxiliary variables
     long Ni = 0, Ns = 0, Nr = 0;

     // Data
     long np = n*l+m;
     const long max_it = 10*np;
     const long max_buffer = np;
     long buffer_it = 0;

     vector <long> Data_i ( max_buffer ), Data_s ( max_buffer ), Data_r ( max_buffer );
     vector <double> Data_t ( max_buffer );

     // Layer statistics
     vector <long> mData_i ( l ), mData_s ( l ), mData_r ( l );
     vector < vector <long> > mbData_i ( l ), mbData_s ( l ), mbData_r ( l );
     for ( long k=0; k<l; k++ ) {
          mbData_i[k].resize ( max_buffer );
          mbData_s[k].resize ( max_buffer );
          mbData_r[k].resize ( max_buffer );

          mData_i[k] = 0;
          mData_s[k] = 0;
          mData_r[k] = 0;
     }


     // Epidemic initialization
     vector <char> processes_state ( n*l );
     for ( long k=0; k<l; k++ ) {
          for ( long i=0; i<n; i++ ) {
               if ( gsl_rng_uniform ( randomic ) < i0 ) {
                    processes_state[i+k*n] = INFECTED;
                    Ni++;

                    mData_i[k]++;

               } else {
                    processes_state[i+k*n] = SUSCEPTIBLE;
                    Ns++;

                    mData_s[k]++;
               }
          }
     }

     // Writing data
     FILE *output = fopen ( fname, "w" );

     // Print info
     printf ( "n = %ld\tm = %ld\n", n, m );

     // Poisson processes initialization
     vector <double> processes_time ( np );
     vector <double> processes_lambda ( np );

     // Lambda rate
     for ( long i=0; i<m; i++ ) {
          if ( M.EL[i].first.second == M.EL[i].second.second ) { // same layer
               processes_lambda[i] = beta;
          } else {
               processes_lambda[i] = eta;
          }
     }

     // Recovery rates
     for ( long i=m; i<np; i++ ) {
          processes_lambda[i] = gamma;
     }

     // Initial time event --> NEW!
     for ( long i=0; i< ( long ) processes_time.size(); i++ ) {
          // aqui

          long id_process_index = i;
          if ( i < m ) { // contact processes
               long from = M.EL[id_process_index].second.first;
               long from_l = M.EL[id_process_index].second.second;

               if ( processes_state[from+n*from_l] == INFECTED ) {

                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                    } while ( rnd == 0 );

                    processes_time[i] = rnd;
               } else {
                    processes_time[i] = Max_double;
               }

          } else {

               // Spontaneous processes
               if ( processes_state[id_process_index-m] == INFECTED ) {
                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                    } while ( rnd == 0 );

                    processes_time[i] = rnd;
               } else {
                    processes_time[i] = Max_double;
               }
          }
     }
     // Initial time event --> NEW!


     // Iterate the dynamics
     double t = 0;
     long it = 0;
     long nimin = 0;//( long ) floor ( n/1000 );
     for ( ; ( it<max_it && Ni > nimin && t <= tmax ); it++ ) {
          // Next process
          long id_process = min_element ( processes_time.begin(), processes_time.end() ) - processes_time.begin();
          t = processes_time[id_process];


          // Prerform the dynamical rules
          if ( id_process < m ) {
               // Contact process
               long to = M.EL[id_process].first.first;
               long from = M.EL[id_process].second.first;

               long to_l = M.EL[id_process].first.second;
               long from_l = M.EL[id_process].second.second;

               if ( processes_state[from+n*from_l] == SUSCEPTIBLE ) {
                    printf ( "E1\n" );
               }

               if ( processes_state[from+n*from_l] == INFECTED && processes_state[to+n*to_l] == SUSCEPTIBLE ) {
                    processes_state[to+n*to_l] = INFECTED;
                    Ni++;
                    Ns--;

                    mData_i[M.EL[id_process].first.second]++;
                    mData_s[M.EL[id_process].first.second]--;

                    // adding new processes --> New!!
                    for ( long i=0; i< ( long ) m; i++ ) {
                         long id_process_index_tmp = i;
                         long from_tmp = M.EL[id_process_index_tmp].second.first;
                         long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                         if ( from_tmp == to && from_l_tmp == to_l ) {

                              double rnd = 0;
                              do {
                                   rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[i] );
                              } while ( rnd == 0 );

                              processes_time[i] = t+rnd;
                         }
                    }

                    double rnd = 0;
                    do {
                         rnd = gsl_ran_exponential ( randomic, 1.0/processes_lambda[m+to+n*to_l] );
                    } while ( rnd == 0 );

                    processes_time[m+to+n*to_l] = t+rnd;
                    // adding new processes --> New!!

               }

               processes_time[id_process] = Max_double; //+= gsl_ran_exponential ( randomic, 1.0/processes_lambda[id_process] );			// rever!!! estava Max_double

          } else {
               // Spontaneous process
               if ( processes_state[id_process-m] == SUSCEPTIBLE ) {
                    printf ( "E2 " );
               }

               if ( processes_state[id_process-m] == INFECTED ) {
                    // SIR
                    processes_state[id_process-m] = RECOVERED;
                    Nr++;
                    Ni--;

                    long layer = ( id_process-m ) /n;

                    mData_i[layer]--;
                    mData_r[layer]++;

                    // removing processes --> NEW!!
                    for ( long i=0; i< ( long ) m; i++ ) {
                         long id_process_index_tmp = i;

                         long from_tmp = M.EL[id_process_index_tmp].second.first;
                         long from_l_tmp = M.EL[id_process_index_tmp].second.second;

                         if ( from_tmp == id_process - m - n*layer && from_l_tmp == layer ) {
                              processes_time[i] = Max_double;
                         }
                    }

                    processes_time[id_process] = Max_double;
                    // removing processes --> NEW!!

               }

          }

          // Print
          if ( it%1000 == 0 ) {
               printf ( "%ld\t%.6e\t%ld\t%ld\t%ld", it, t, Ns, Ni, Nr );

               for ( long k=0; k<l; k++ ) {
                    printf ( "\t%ld\t%ld\t%ld\t%ld", mData_i[k], mData_s[k], mData_r[k], ( mData_i[k]+mData_s[k]+mData_r[k] ) );
               }

               printf ( "\n" );
          }

          // Saving data
          Data_i[buffer_it] = Ni;
          Data_s[buffer_it] = Ns;
          Data_r[buffer_it] = Nr;
          Data_t[buffer_it] = t;

          for ( long k=0; k<l; k++ ) {
               mbData_i[k][buffer_it] = mData_i[k];
               mbData_s[k][buffer_it] = mData_s[k];
               mbData_r[k][buffer_it] = mData_r[k];
          }

          buffer_it++;

          if ( buffer_it == max_buffer ) {
               for ( long i=0 ; i<max_buffer; i++ ) {
                    fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

                    for ( long k=0; k<l; k++ ) {
                         fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
                    }

                    fprintf ( output, "\n" );
               }
               fflush ( output );

               buffer_it = 0;
          }

     }

     // Writing data
     for ( long i=0 ; i<buffer_it-1; i++ ) {
          fprintf ( output, "%.6e\t%ld\t%ld\t%ld", Data_t[i], Data_s[i], Data_i[i], Data_r[i] );

          for ( long k=0; k<l; k++ ) {
               fprintf ( output, "\t%ld\t%ld\t%ld", mbData_i[k][i], mbData_s[k][i], mbData_r[k][i] );
          }

          fprintf ( output, "\n" );
     }
     fclose ( output );

     char state_name[256];
     sprintf ( state_name, "States_%s", fname );
     Write_State ( processes_state, state_name );

     return 1;
}
