#ifndef Markov_H
#define Markov_H

#define GSL_LOG_DBL_MIN   (-7.0839641853226408e+02)
#define GSL_LOG_DBL_MAX    7.0978271289338397e+02


#include <omp.h>
#include <float.h>
#include <math.h>

#include "Multilayer.h"

typedef struct sir_t {
    double **p;
    double **r;
    } sir;


const double eps = DBL_EPSILON;

double activity ( const Multilayer &M, long i,  int layer,  double lambda );

int Markov_SIS_PostProcessing ( const Multilayer &Ml, long Steps, double **p, FILE *fpf, FILE *fp_out );
int Markov_SIR_PostProcessing ( const Multilayer &Ml, long Steps, sir p_sir, FILE *fpf, FILE *fp_out );

double** Markov_SIS ( const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma );
double** Markov_SIS_Reinfection ( const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma );

sir Markov_SIR (  const Multilayer &Ml, long Steps, double p0, double lambda, double delta, double eta, double gamma );

#endif
